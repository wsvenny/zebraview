#!/usr/bin/env bash

if [ "$1" = "install" ]; then
    echo "Install mode"

    virtualenv -p /usr/bin/python3 --no-site-packages --distribute .env \
     && source .env/bin/activate \
     && pip install -r requirements.txt \
     && echo "OK" \
     || exit 1
fi

if [ ! -f .env/bin/activate ]; then
    echo "Virtualenv is not ready! Run this script as SU with parameter 'install' first."
    exit 1
fi

source .env/bin/activate

echo "Starting server..."
python manage.py runserver
