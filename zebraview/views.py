from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.http import require_GET, require_POST, require_http_methods
from django.contrib.auth.decorators import login_required
from zebraview.forms import FORMS
from zebraview.models import MODELS
from django.db.models import Q

from django.db import models


# welcome page
@login_required
@require_GET
def ep_main(request):
    return render(request, "main.html", {"page_title": "Main page"})


# list
@login_required
@require_GET
def ep_entities(request, what):
    objects = MODELS[what].objects.all()
    attr_names = None
    if objects:
        attr_names = [f.name for f in objects[0]._meta.get_fields() if not isinstance(f, models.ManyToOneRel)]

    data = [{name: obj.__getattribute__(name) for name in attr_names} for obj in objects]

    return render(request, 'list.html', {"page_title": "List of {}s".format(what), "what": what,
                                         'attr_names': attr_names, 'data': data})


# autocomplete
@login_required
@require_GET
def ep_autocomplete(request, what):
    term = request.GET.get('term')
    if what in {"room", "fishline"}:
        if term:
            q = MODELS[what].objects.filter(Q(name__icontains=term) | Q(id__icontains=term))
        else:
            q = MODELS[what].objects.all()
        q = q.order_by("name")[:15]
        data = [{"id": d.id, "label": "{} ({} #{})".format(d.name, what, d.id)} for d in q]

    else:
        if term:
            q = MODELS[what].objects.filter(id__icontains=term)
        else:
            q = MODELS[what].objects.all()
        q = q.order_by("-id")[:15]
        data = [{"id": d.id, "label": "{} #{}".format(what, d.id)} for d in q]
    return JsonResponse(data, safe=False)


# autocomplete feedback
@login_required
@require_GET
def ep_autocomplete_feedback(request, what, instance):
    obj = get_object_or_404(MODELS[what], id=instance)
    if what in {"room", "fishline"}:
        data = {"id": obj.id, "label": "{} ({} #{})".format(obj.name, what, obj.id)}

    else:
        data = {"id": obj.id, "label": "{} #{}".format(what, obj.id)}
    return JsonResponse(data, safe=False)


# create
@login_required
@require_http_methods(['GET', 'POST'])
def ep_entity_new(request, what):
    # render form
    if request.method == 'GET':
        return render(request, "edit.html", {"page_title": "Create new {}".format(what), "what": what,
                                             'form': FORMS[what]()})
    # create new entity
    else:
        form = FORMS[what](request.POST)
        if form.is_valid():
            obj = form.save()
            return redirect('/{}s/{}'.format(what, obj.id))
        return render(request, "edit.html", {"page_title": "Create new {}".format(what), "what": what,
                                             'form': form})


# detail
@login_required
@require_GET
def ep_entity_instance(request, what, instance):
    obj = get_object_or_404(MODELS[what], id=instance)
    attr_names = [f.name for f in obj._meta.get_fields() if not isinstance(f, models.ManyToOneRel)]
    data = {name: obj.__getattribute__(name) for name in attr_names}
    return render(request, 'detail.html', {'data': data, 'attr_names': attr_names,
                                           'page_title': "{} #{}".format(what, instance), 'what': what})


# edit
@login_required
@require_http_methods(['GET', 'POST'])
def ep_entity_instance_edit(request, what, instance):
    # render form
    obj = get_object_or_404(MODELS[what], id=instance)
    if request.method == 'GET':
        attr_names = [f.name for f in obj._meta.get_fields() if not isinstance(f, models.ManyToOneRel)]
        data = {name: obj.__getattribute__(name) for name in attr_names}
        return render(request, "edit.html", {"page_title": "Edit {} #{}".format(what, instance), "what": what,
                                             'form': FORMS[what](instance=obj, initial=data)})
    # save changes
    else:
        if not request.user.is_authenticated:
            return HttpResponseForbidden("Log in to do this.")
        form = FORMS[what](request.POST or None, instance=obj)
        if form.is_valid():
            form.save()
            return redirect('/{}s/{}'.format(what, obj.id))
        return render(request, "edit.html", {"page_title": "Edit {} #{}".format(what, instance), "what": what,
                                             'form': form})


# delete
@login_required
@require_POST
def ep_entity_instance_delete(request, what, instance):
    get_object_or_404(MODELS[what], id=instance).delete()
    return redirect("/{}s".format(what))
