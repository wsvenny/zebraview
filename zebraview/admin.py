from django.contrib import admin
import zebraview.models as models


admin.site.register(models.Room)
admin.site.register(models.Rack)
admin.site.register(models.Fishline)
admin.site.register(models.Stock)
admin.site.register(models.Substock)
admin.site.register(models.Position)
