from django.db import models
from django.forms import ModelForm
from django import forms
from zebraview.models import Position, Stock, Substock, Fishline, Rack, Room


def get_fields_list(model):
    return [f.name for f in model._meta.get_fields() if not isinstance(f, (models.AutoField,
                                                                           models.OneToOneRel,
                                                                           models.ManyToOneRel,
                                                                           models.ManyToManyRel))]


def autocomplete_widget():
    return forms.TextInput(attrs={'class': 'autocomplete'})


class PositionForm(ModelForm):
    class Meta:
        model = Position
        fields = get_fields_list(model)
        widgets = {
            'id_rack': autocomplete_widget()
        }


class RoomForm(ModelForm):
    class Meta:
        model = Room
        fields = get_fields_list(model)


class RackForm(ModelForm):
    class Meta:
        model = Rack
        fields = get_fields_list(model)
        widgets = {
            'id_room': autocomplete_widget()
        }


class FishlineForm(ModelForm):
    class Meta:
        model = Fishline
        fields = get_fields_list(model)


class StockForm(ModelForm):
    class Meta:
        model = Stock
        fields = get_fields_list(model)
        widgets = {
            'id_fishline': autocomplete_widget(),
            'date_of_birth': forms.DateInput(attrs={'type': 'date'})
        }


class SubstockForm(ModelForm):
    class Meta:
        model = Substock
        fields = get_fields_list(model)
        widgets = {
            'id_stock': autocomplete_widget(),
            'id_position': autocomplete_widget()
        }


FORMS = {'room': RoomForm, 'rack': RackForm, 'stock': StockForm, 'substock': SubstockForm, 'position': PositionForm, 'fishline': FishlineForm}
