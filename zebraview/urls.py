from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.decorators import login_required
from zebraview import views


urlpatterns = [
    path('', views.ep_main),
    path('login', LoginView.as_view(template_name="login.html")),
    path('logout', login_required(LogoutView.as_view())),
    path('<str:what>s', views.ep_entities),
    path('autocomplete/<str:what>s', views.ep_autocomplete),
    path('autocomplete/<str:what>s/<int:instance>', views.ep_autocomplete_feedback),
    path('<str:what>s/new', views.ep_entity_new),
    path('<str:what>s/<int:instance>', views.ep_entity_instance),
    path('<str:what>s/<int:instance>/edit', views.ep_entity_instance_edit),
    path('<str:what>s/<int:instance>/delete', views.ep_entity_instance_delete),
]
