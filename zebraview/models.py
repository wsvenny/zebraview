from django.db import models

# Create your models here.


class Room(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=2000, null=True, default=None)

    class Meta:
       db_table = "room"


class Rack(models.Model):
    id_room = models.ForeignKey('Room', models.CASCADE, verbose_name='room', help_text='select room')
    columns = models.IntegerField()
    rows = models.IntegerField()

    class Meta:
        db_table = "rack"


class Position(models.Model):
    id_rack = models.ForeignKey('Rack', models.CASCADE, verbose_name='rack', help_text='select rack')
    column = models.IntegerField()
    row = models.IntegerField()

    class Meta:
        db_table = "position"
        unique_together = ('id_rack', 'column', 'row',)


class Fishline(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=2000, null=True, default=None)

    class Meta:
        db_table = "fishline"


class Stock(models.Model):
    id_fishline = models.ForeignKey('Fishline', models.CASCADE, verbose_name='fishline', help_text='select fishline')
    date_of_birth = models.DateTimeField()

    class Meta:
        db_table = "stock"


class Substock(models.Model):
    id_stock = models.ForeignKey('Stock', models.CASCADE, verbose_name='stock', help_text='select stock')
    id_position = models.OneToOneField('Position', models.CASCADE, verbose_name='position', help_text='set position')
    count = models.IntegerField()

    class Meta:
        db_table = "substock"


MODELS = {'substock': Substock, 'stock': Stock, 'room': Room, 'rack': Rack, 'position': Position, 'fishline': Fishline}
