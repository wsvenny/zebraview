from django.apps import AppConfig


class ZebraviewConfig(AppConfig):
    name = 'zebraview'
