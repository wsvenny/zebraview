from datetime import datetime


def startswith(text, start):
    return str(text).startswith(start)


def endswith(text, end):
    return str(text).endswith(end)


def render_datetime(value, output_format='%-d.%-m.%Y %H:%M'):
    if isinstance(value, str):
        try:
            value = datetime.strptime(value.split('.')[0], '%Y-%m-%dT%H:%M:%S')
        except:
            return value
    return value.strftime(output_format)


def nice_attribute_name(value):
    if value.startswith("id_"):
        value = value[3:]
    return value.replace("_", " ")
