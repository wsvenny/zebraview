from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import reverse
from jinja2 import Environment

from zebraview.jinja2.filters import startswith, endswith, render_datetime, nice_attribute_name


def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
    })
    env.filters.update({
        'startswith': startswith,
        'endswith': endswith,
        'datetime': render_datetime,
        'nice_attr_name': nice_attribute_name
    })
    return env
