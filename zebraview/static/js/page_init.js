$(document).ready(function () {
    $(".autocomplete").each(function () {
        var val = this;
        var auto_id = 'autocomplete_' + $(val).attr('name');
        $(val).hide().parent().prepend('<input type="text" id="' + auto_id + '" />');
        if($(val).val()) {
            $.getJSON("/autocomplete/" + $(val).attr('name').substr(3) + "s/" + $(val).val(), function (data) {
                $("#" + auto_id).val(data.label);
            });
        }
        $("#" + auto_id).autocomplete({
            source: "/autocomplete/" + $(val).attr('name').substr(3) + "s",
            minLength: 1,
            select: function(event, ui) {
                $(val).val(ui.item.id);
            }
        });
    });

    $(".confirm").submit(function () {
        if(!confirm('Are you sure?'))
            return false;
    });
});